# Confused Dealership and Maintenance

Team:
* Dylan Minton - Sales Microservice
* Gordon Tran - Services Microservice

## How to run this project
1. Clone this repository

    > git clone https://gitlab.com/thedylanminton/car-car

2. Change into the project directory

    > cd Projects\project-beta

3. Start up Docker to run and build the containers

    > docker volume create beta-data

    > docker-compose build

    > docker-compose up

4. Access the website at:

    > http://localhost:3000


## Diagram
![Img](ReadME Pic.png)
https://excalidraw.com/#json=imLGGvau48nkC29xX3E5J,QmZGswrMfJKc7bGrnHhLVQ


## Objective
The objective of this project was to create fictional car dealership that also offers maintenance as well.<br />
There are 3 different microservices in this project:
- Inventory
- Sales
- Services

**Inventory** dealt with adding cars to the dealership's inventory.<br />
This included:
- Manufacturers
- Models
- Automobiles

**Sales** dealt with the transactional side of the project.<br />
This included:
- Adding a sale
- Customers
- Salesperson

**Services** dealt with the mechanical service side of the project.<br />
This included:
- Adding an appointment
- Technicians

## Inventory microservice
Inventory handled on: Port 8100

- Manufacturers: Create, List
- Models: Create, List
- Automobiles: Add, List

## Service microservice
Service handled on: Port 8080

- Technicians: Create, Delete*
- Appointments: Create, Update, Delete*<br />
*These functions are not allowed on the website and only accessed through Insomnia

Service History: A log of all the appointments made

Models:
- Technician
- Appointment
- AutomobileVO
    > Polls Inventory for Automobile Updates

## Sales microservice

Sales handled on: Port 8090

- Salesperson: Create, Delete*
- Customer: Create, Delete*
- Sale: Create, Delete*<br />
*These functions are not allowed on the website and only accessed through Insomnia

Salesperson History: A log of all the sales made

Models:
- Salesperson
- Customer
- Sale
- AutomobileVO
    > Polls Inventory for Automobile Updates

## Unique Inputs

The following should be unique from previous inputs for valid form submission

- Customer phone numbers
- Automobile VINs
- Employee IDs
