import React, { useState } from 'react';
function AutomobilesForm({ models, getAutomobiles }) {
    const [formData, setFormData] = useState({
        color: '',
        year: '',
        vin: '',
        model: '',
    })

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = 'http://localhost:8100/api/automobiles/';

    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      //The single formData object
      //also allows for easier clearing of data
      setFormData({
        color: '',
        year: '',
        vin: '',
        model: '',
      });
      document.getElementById("create-automobile-form").reset();
      getAutomobiles();
    }
  }

  //Notice that we can also replace multiple form change
  //eventListener functions with one
  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    //We can condense our form data event handling
    //into on function by using the input name to update it

    setFormData({
      //Previous form data is spread (i.e. copied) into our new state object
      ...formData,

      //On top of the that data, we add the currently engaged input key and value
      [inputName]: value
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add an Automobile</h1>
          <form onSubmit={handleSubmit} id="create-automobile-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Year" required type="number" name="year" id="year" className="form-control" />
              <label htmlFor="year">Year</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleFormChange} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
                <label htmlFor="vin">VIN</label>
            </div>
            <div className="mb-3">
                <select onChange={handleFormChange} required name="model_id" id="model_id" className="form-select">
                    <option value="">Choose a model...</option>

                    {models.map(model => {
                        return (
                            <option key={model.id} value={model.id}>{model.name}</option>
                        )
                })}
                </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default AutomobilesForm;
