function ModelsList({ models }) {
    return(
        <>
            <h1>Models</h1>
            <table className="table table-striped my-3 ">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(model => {
                        return(
                            <tr key={model.id}>
                                <td>{ model.name }</td>
                                <td>{ model.manufacturer.name }</td>
                                <td><img className="w-25" src={model.picture_url} /></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    )
}

export default ModelsList;
