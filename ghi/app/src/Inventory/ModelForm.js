import React, { useState } from 'react';

function ModelForm({ manufacturers, getModels }) {
    const [ name, setName ] = useState('');
    const [ pictureUrl, setPictureUrl ] = useState('');
    const [ manufacturer, setManufacturer ] = useState('');

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            name,
            picture_url: pictureUrl,
            manufacturer_id: manufacturer,
        };

        const modelUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(modelUrl, fetchConfig);
        if (response.ok) {
            setName('');
            setPictureUrl('');
            setManufacturer('');
            getModels();
        }
    }

    function handleChangeName(event) {
        const { value } = event.target;
        setName(value);
    }
    function handleChangePictureUrl(event) {
      const { value } = event.target;
      setPictureUrl(value);
    }
    function handleChangeManufacturer(event) {
      const { value } = event.target;
      setManufacturer(value);
    }

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a Model</h1>
            <form onSubmit={handleSubmit} id="create-model-form">
              <div className="form-floating mb-3">
                <input value={name} onChange={handleChangeName} placeholder="Model Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Model name...</label>
              </div>
              <div className="form-floating mb-3">
                <input value={pictureUrl} onChange={handleChangePictureUrl} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" className="form-control" />
                <label htmlFor="picture_url">Picture URL...</label>
              </div>
              <div className="mb-3">
                <select value={manufacturer} onChange={handleChangeManufacturer} required name="manufacturer" id="manufacturer" className="form-select">
                    <option value="">Choose a manufacturer...</option>
                    {manufacturers.map(manufacturer => {
                      return (
                        <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                      )
                    })}
                  </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default ModelForm;
