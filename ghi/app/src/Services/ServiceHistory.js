import React, { useState } from "react";

function ServiceHistory({ automobiles, technicians, appointments }) {
    const [vin, setVin] = useState('');

    const handleVINChange = (event) => {
        const value = event.target.value; //check dev console
        setVin(value);
    }

    function isVIP(appointment){
    //for every car
    //if there is a car.vin === appointment.vin
    //vipStatus is true
    for(let automobile of automobiles){
    if(automobile.vin === appointment.vin)
        return "Yes";
    }
    return "No"
    }

    return (
        <div className="row">
            <div className="mt-4 col-10">
                <h1>Service History</h1>
                <div>
                    <label htmlFor="vin_input">Enter VIN:</label>
                    <input type="text" id="vin_input" onChange={handleVINChange} />
                </div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Is VIP?</th>
                            <th>Customer Name</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        {appointments.map((appointment, id)=>{
                            const date = new Date(appointment.date_time)
                            const dateOnly = date.toLocaleDateString()
                            const timeOnly = date.toLocaleTimeString([],{ hour: "2-digit", minute: "2-digit" })
                            const technician = technicians.find((tech) => tech.employee_id === appointment.technician);
                            if(vin === '' || appointment.vin.toUpperCase().includes(vin.toUpperCase())){
                                return(
                                    <tr key={id} value={appointment.id}>
                                    <td>{appointment.vin}</td>
                                    <td>{isVIP(appointment)}</td>
                                    <td>{appointment.customer}</td>
                                    <td>{dateOnly}</td>
                                    <td>{timeOnly}</td>
                                    <td>{technician.first_name} {technician.last_name}</td>
                                    <td>{appointment.reason}</td>
                                    <td>{ appointment.status }</td>
                                </tr>
                                )
                            }
                            else
                                return null
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    )
}
export default ServiceHistory
