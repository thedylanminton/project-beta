import React, { useState } from 'react';
function AppointmentForm({ technicians, getAppointments }) {
    const [formData, setFormData] = useState({
        date_time: '',
        reason: '',
        status: 'created',
        vin: '',
        customer: '',
        technician: '',
    })

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = 'http://localhost:8080/api/appointments/';

    const fetchConfig = {
      method: "post",
      //Because we are using one formData state object,
      //we can now pass it directly into our request!
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      //The single formData object
      //also allows for easier clearing of data
      setFormData({
        date_time: '',
        reason: '',
        status: 'created',
        vin: '',
        customer: '',
        technician: '',
      });
      document.getElementById("create-appointment-form").reset();
      getAppointments();
    }
    const userTimezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    const userDateTime = new Date(formData.date_time);
    const userDateTimeString = userDateTime.toLocaleString('en-US', {
      timeZone: userTimezone,
    });
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,
      [inputName]: value
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create an appointment</h1>
          <form onSubmit={handleSubmit} id="create-appointment-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Automobile VIN" required type="text" name="vin" id="vin" className="form-control" />
              <label htmlFor="vin">Automobile VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Customer Name" required type="text" name="customer" id="customer" className="form-control" />
              <label htmlFor="customer">Customer Name</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleFormChange} placeholder="Date and Time" required type="datetime-local" name="date_time" id="date_time" className="form-control" />
                <label htmlFor="date_time">Date and Time</label>
            </div>
            <div className="mb-3">
                <select onChange={handleFormChange} required name="technician" id="technician" className="form-select">
                    <option value="">Choose a technician</option>

                    {technicians.map(technician => {
                        return (
                            <option key={technician.employee_id} value={technician.employee_id}>{technician.first_name} {technician.last_name}</option>
                        )
                })}
                </select>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
              <label htmlFor="reason">Reason</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default AppointmentForm;
