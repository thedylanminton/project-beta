function AppointmentList({ appointments, technicians, automobiles, getAppointments }) {
    function isVIP(appointment){
        //for every car
        //if there is a car.vin === appointment.vin
        //vipStatus is true
      for(let automobile of automobiles){
        if(automobile.vin === appointment.vin)
          return "Yes";
      }
      return "No"
    }

    async function cancelApp(id){
      const url = `http://localhost:8080/api/appointments/${id}/cancel`
      const fetchConfig = {
        method: "PUT",
        headers: {
          'Content-Type': 'application/json',
        },
      }
      const response = await fetch(url, fetchConfig)
      if (response.ok){
        console.log("cancelled")

      }
      getAppointments()
    }

    async function finishApp(id){
      const url = `http://localhost:8080/api/appointments/${id}/finish`
      const fetchConfig = {
        method: "PUT",
        headers: {
          'Content-Type': 'application/json',
        },
      }
      const response = await fetch(url, fetchConfig)
      if (response.ok){
        console.log("finished")
      }
      getAppointments()
    }

    return(
        <>
            <h1>Service Appointments</h1>
            <table className="table table-striped my-3">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.filter((appointment) => appointment.status !== "finished" && appointment.status !== "cancelled").map((appointment) => {
                          const date = new Date(appointment.date_time)
                          date.setHours(date.getHours()+8) //this makes it so it's PST
                          const dateOnly = date.toLocaleDateString()
                          const technician = technicians.find((tech) => tech.employee_id === appointment.technician);

                          const timeOnly = date.toLocaleTimeString([],{ hour: "2-digit", minute: "2-digit" })
                          return(
                              <tr key={appointment.id}>
                                  <td>{ appointment.vin }</td>
                                  <td>{isVIP(appointment)}</td>
                                  <td>{ appointment.customer }</td>
                                  <td>{ dateOnly }</td>
                                  <td>{ timeOnly }</td>
                                  <td>{technician.first_name} {technician.last_name}</td>
                                  <td>{ appointment.reason }</td>
                                  <td>
                                    <button onClick = {()=>cancelApp(appointment.id)} className='btn btn-danger'>Cancel</button>
                                    <button onClick = {()=>finishApp(appointment.id)} className='btn btn-success'>Finish</button>
                                  </td>
                              </tr>
                          );
                        })}
                </tbody>
            </table>
        </>
    )
}

export default AppointmentList;
