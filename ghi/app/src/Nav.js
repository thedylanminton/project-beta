import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">Home</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Inventory
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <li>
                  <NavLink className="dropdown-item" to="inventory/manufacturers">Manufacturers</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="inventory/manufacturers/new">Add a Manufacturer</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="inventory/models">Models</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="inventory/models/new">Add a Model</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="inventory/automobiles">Automobiles</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="inventory/automobiles/new">Add an Automobile</NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="salespeople">Salespeople</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="salespeople/new">Add a Salesperson</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="customers">Customers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="customers/new">Add a Customer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="sales">Sales</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="sales/new">Add a Sale</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="sales/history">Salesperson History</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="technicians">Technicians</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="technicians/new">Add a technician</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="appointments">Appointments</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="appointments/new">Add an appointment</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="appointments/history">Check service history</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
