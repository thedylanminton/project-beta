import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useState, useEffect } from 'react';

import MainPage from './MainPage';
import Nav from './Nav';

// Inventory
import ManufacturerList from './Inventory/ManufacturerList';
import ManufacturerForm from './Inventory/ManufacturerForm';
import ModelsList from './Inventory/ModelsList';
import ModelForm from './Inventory/ModelForm';
import AutomobilesForm from './Inventory/AutomobilesForm';
import AutomobileList from './Inventory/AutomobileList';

// Sales
import SalespersonForm from './Sales/SalespersonForm';
import SalespeopleList from './Sales/SalespeopleList';
import CustomerForm from './Sales/CustomerForm';
import CustomerList from './Sales/CustomerList';
import SaleForm from './Sales/SaleForm';
import SalesList from './Sales/SalesList';
import SalespersonHistory from './Sales/SalespersonHistory';

// Services
import TechnicianForm from './Services/TechnicianForm';
import TechnicianList from './Services/TechnicianList';
import AppointmentForm from './Services/AppointmentForm';
import AppointmentList from './Services/AppointmentList';
import ServiceHistory from './Services/ServiceHistory';

function App() {
  // Data Fetching
  const [ automobiles, setAutomobiles ] = useState([]);
  const [ manufacturers, setManufacturers ] = useState([]);
  const [ models, setModels ] = useState([]);
  const [ customers, setCustomers ] = useState([]);
  const [ salespeople, setSalespeople ] = useState([]);
  const [ sales, setSales ] = useState([]);
  const [ technicians, setTechnicians ] = useState([]);
  const [ appointments, setAppointments ] = useState([]);

  async function getAutomobiles() {
    const response = await fetch('http://localhost:8100/api/automobiles/');
    if (response.ok) {
      const { autos } = await response.json();
      setAutomobiles(autos);
    } else {
      console.error('An error occurred fetching the data');
    }
  }
  async function getManufacturers() {
    const response = await fetch('http://localhost:8100/api/manufacturers/');
    if (response.ok) {
      const { manufacturers } = await response.json();
      setManufacturers(manufacturers);
    } else {
      console.error('An error occurred fetching the data');
    }
  }
  async function getModels() {
    const response = await fetch('http://localhost:8100/api/models/');
    if (response.ok) {
      const { models } = await response.json();
      setModels(models);
    } else {
      console.error('An error occurred fetching the data');
    }
  }
  async function getCustomers() {
    const response = await fetch('http://localhost:8090/api/customers/');
    if (response.ok) {
      const { customers } = await response.json();
      setCustomers(customers);
    } else {
      console.error('An error occurred fetching the data');
    }
  }
  async function getSalespeople() {
    const response = await fetch('http://localhost:8090/api/salespeople/');
    if (response.ok) {
      const { salespeople } = await response.json();
      setSalespeople(salespeople);
    } else {
      console.error('An error occurred fetching the data');
    }
  }
  async function getSales() {
    const response = await fetch('http://localhost:8090/api/sales/');
    if (response.ok) {
      const { sales } = await response.json();
      setSales(sales);
    } else {
      console.error('An error occurred fetching the data');
    }
  }
  async function getTechnicians() {
    const response = await fetch('http://localhost:8080/api/technicians/');
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    } else {
      console.error('An error occurred fetching the data');
    }
  }
  async function getAppointments() {
    const response = await fetch('http://localhost:8080/api/appointments/');
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    } else {
      console.error('An error occurred fetching the data');
    }
  }

  useEffect(() => {
    getAutomobiles();
    getManufacturers();
    getModels();
    getCustomers();
    getSalespeople();
    getSales();
    getTechnicians();
    getAppointments();
  }, [])

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="salespeople">
            <Route path="" element={<SalespeopleList salespeople={salespeople} />} />
            <Route path="new" element={<SalespersonForm getSalespeople={getSalespeople} />} />
          </Route>
          <Route path="customers">
            <Route path="" element={<CustomerList customers={customers} />} />
            <Route path="new" element={<CustomerForm getCustomers={getCustomers} />} />
          </Route>
          <Route path="technicians">
            <Route path="" element={<TechnicianList technicians={technicians} />} />
            <Route path="new" element={<TechnicianForm getTechnicians={getTechnicians} />} />
          </Route>
          <Route path="appointments">
            <Route path="" element={<AppointmentList appointments={appointments} technicians={technicians} automobiles={automobiles} getAppointments={getAppointments} />} />
            <Route path="new" element={<AppointmentForm technicians={technicians} getAppointments={getAppointments} />} />
            <Route path="history" element={<ServiceHistory automobiles={automobiles} technicians={technicians} appointments={appointments} />} />
          </Route>
          <Route path="sales">
            <Route path="" element={<SalesList sales={sales} />} />
            <Route path="new" element={<SaleForm automobiles={automobiles} customers={customers} salespeople={salespeople} getSales={getSales} getAutomobiles={getAutomobiles} />} />
            <Route path="history" element={<SalespersonHistory salespeople={salespeople} sales={sales} />} />
          </Route>
          <Route path="inventory">
            <Route path="manufacturers">
              <Route path="" element={<ManufacturerList manufacturers={manufacturers} />} />
              <Route path="new" element={<ManufacturerForm getManufacturers={getManufacturers} />} />
            </Route>
            <Route path="models">
              <Route path="" element={<ModelsList models={models} />} />
              <Route path="new" element={<ModelForm manufacturers={manufacturers} getModels={getModels} />} />
            </Route>
            <Route path="automobiles">
              <Route path="" element={<AutomobileList automobiles={automobiles} />} />
              <Route path="new" element={<AutomobilesForm models={models} getAutomobiles={getAutomobiles} />} />
            </Route>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
