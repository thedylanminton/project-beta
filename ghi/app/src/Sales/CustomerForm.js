import React, { useState } from 'react';

function CustomerForm({ getCustomers }) {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [address, setAddress] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            first_name: firstName,
            last_name: lastName,
            address: address,
            phone_number: phoneNumber,
        };

        const customerUrl = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
            setFirstName('');
            setLastName('');
            setAddress('');
            setPhoneNumber('');
            getCustomers();
        }
    }

    function handleChangeFirstName(event) {
        const { value } = event.target;
        setFirstName(value);
    }
    function handleChangeLastName(event) {
        const { value } = event.target;
        setLastName(value);
    }
    function handleChangeAddress(event) {
        const { value } = event.target;
        setAddress(value);
    }
    function handleChangePhoneNumber(event) {
        const { value } = event.target;
        setPhoneNumber(value);
    }

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a Customer</h1>
            <form onSubmit={handleSubmit} id="create-customer-form">
              <div className="form-floating mb-3">
                <input value={firstName} onChange={handleChangeFirstName} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" />
                <label htmlFor="first_name">First name...</label>
              </div>
              <div className="form-floating mb-3">
                <input value={lastName} onChange={handleChangeLastName} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" />
                <label htmlFor="last_name">Last name...</label>
              </div>
              <div className="form-floating mb-3">
                <input value={address} onChange={handleChangeAddress} placeholder="Address" required type="text" name="address" id="address" className="form-control" />
                <label htmlFor="address">Address...</label>
              </div>
              <div className="form-floating mb-3">
                <input value={phoneNumber} onChange={handleChangePhoneNumber} placeholder="Phone Number" required type="text" name="phone_number" id="phone_number" className="form-control" />
                <label htmlFor="phone_number">Phone number...</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default CustomerForm;
