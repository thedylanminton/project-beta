import React, { useState } from 'react';

function SaleForm({ automobiles, salespeople, customers, getSales, getAutomobiles }) {
    const [ automobile, setAutomobile ] = useState('');
    const [ salesperson, setSalesperson ] = useState('');
    const [ customer, setCustomer ] = useState('');
    const [ price, setPrice ] = useState('');

    async function handleSubmit(event) {
        event.preventDefault();

        const automobileSoldData = {
          "sold": true
        }

        const automobileUrl = `http://localhost:8100/api/automobiles/${automobile}/`;
        const automobileFetchConfig = {
          method: "put",
          body: JSON.stringify(automobileSoldData),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        await fetch(automobileUrl, automobileFetchConfig);

        const saleData = {
            automobile,
            salesperson,
            customer,
            price,
        };

        const salesUrl = 'http://localhost:8090/api/sales/';
        const saleFetchConfig = {
            method: "post",
            body: JSON.stringify(saleData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const saleResponse = await fetch(salesUrl, saleFetchConfig);
        if (saleResponse.ok) {
            setAutomobile('');
            setSalesperson('');
            setCustomer('');
            setPrice('');
            getSales();
            getAutomobiles();
        }
    }

    function handleChangeAutomobile(event) {
        const { value } = event.target;
        setAutomobile(value);
    }
    function handleChangeSalesperson(event) {
        const { value } = event.target;
        setSalesperson(value);
    }
    function handleChangeCustomer(event) {
        const { value } = event.target;
        setCustomer(value);
    }
    function handleChangePrice(event) {
        const { value } = event.target;
        setPrice(value);
    }

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Record a New Sale</h1>
            <form onSubmit={handleSubmit} id="create-sale-form">
              <div className="mb-3">
                <select value={automobile} onChange={handleChangeAutomobile} required name="automobile" id="automobile" className="form-select">
                  <option value="">Choose an automobile VIN...</option>
                  {automobiles.map(automobile => {
                      if (automobile.sold === false) {
                        return (
                          <option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>
                        );
                      }
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select value={salesperson} onChange={handleChangeSalesperson} required name="salesperson" id="salesperson" className="form-select">
                  <option value="">Choose a salesperson...</option>
                  {salespeople.map(salesperson => {
                    return (
                      <option key={salesperson.employee_id} value={salesperson.employee_id}>{salesperson.first_name} {salesperson.last_name}</option>
                    )
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select value={customer} onChange={handleChangeCustomer} required name="customer" id="customer" className="form-select">
                  <option value="">Choose a customer...</option>
                  {customers.map(customer => {
                    return (
                      <option key={customer.phone_number} value={customer.phone_number}>{customer.first_name} {customer.last_name}</option>
                    )
                  })}
                </select>
              </div>
              <div className="form mb-3">
                <label htmlFor="price">Price</label>
                <input value={price} onChange={handleChangePrice} placeholder="$0" required type="text" name="price" id="price" className="form-control" />
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default SaleForm;
