function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">Confused Dealership and Maintenance</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          We sell cars and do maintenance.
          <img src = "https://i.gifer.com/VWB.gif" alt="spinning rainbow car gif"></img>
        </p>
      </div>
    </div>
  );
}

export default MainPage;
