from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Technician, Appointment, AutomobileVO


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
       "vin",
       "sold",
    ]


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]


class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "vin",
        "status",
        "customer",
        "technician",
        "id",
    ]
    encoders = {
        "technician": TechnicianDetailEncoder()
    }

    def get_extra_data(self, obj):
        return {"technician": obj.technician.employee_id}




@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianDetailEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"message": "Could not create the technician"},
                status=400
            )

@require_http_methods(["GET", "DELETE"])
def api_technicians(request, pk):
    if request.method == "GET":
        try:
            technicians = Technician.objects.get(pk=pk)
            return JsonResponse(
                {"technicians": technicians},
                encoder=TechnicianDetailEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician not found"},
                status=404
            )
    else:
        count, _ = Technician.objects.filter(pk=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)


        try:
            employee_id = content["technician"]
            print(employee_id)
            technician = Technician.objects.get(employee_id=employee_id)
            content["technician"] = technician
            print("+++++++++++++++++++")
            print(content["technician"])
            print(technician)
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                        appointment,
                        encoder=AppointmentListEncoder,
                        safe=False
                    )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid appointment"},
                status=400,
            )

@require_http_methods(["GET", "DELETE"])
def api_appointments(request, pk):
    if request.method == "GET":
        try:
            appointments = Appointment.objects.get(pk=pk)
            return JsonResponse(
                {"appointments": appointments},
                encoder=AppointmentListEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment not found"},
                status=404
            )
    else:
        count, _ = Appointment.objects.filter(pk=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )


@require_http_methods(["PUT"])
def api_cancel_appointment(request, pk):
    if request.method == "PUT":
        appointment = Appointment.objects.get(pk=pk)
        appointment.status = "cancelled"
        appointment.save()
        return JsonResponse(
            {"appointments": appointment},
            encoder=AppointmentListEncoder,
            safe=False,
        )


@require_http_methods(["PUT"])
def api_finish_appointment(request, pk):
    if request.method == "PUT":
        appointment = Appointment.objects.get(pk=pk)
        appointment.status = "finished"
        appointment.save()
        return JsonResponse(
            {"appointments": appointment},
            encoder=AppointmentListEncoder,
            safe=False,
        )
