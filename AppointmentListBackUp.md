<!--
Backup code for Appointment list

import React, { useEffect, useState } from 'react';
function AppointmentForm() {
    const [technicians, setTechnicians] = useState([])

    const [formData, setFormData] = useState({
        date_time: '',
        reason: '',
        status: 'created',
        vin: '',
        customer: '',
        technician: '',
    })

  const fetchData = async () => {
    const url = 'http://localhost:8080/api/technicians/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  }
  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = 'http://localhost:8080/api/appointments/';

    const fetchConfig = {
      method: "post",
      //Because we are using one formData state object,
      //we can now pass it directly into our request!
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      //The single formData object
      //also allows for easier clearing of data
      setFormData({
        date_time: '',
        reason: '',
        status: 'created',
        vin: '',
        customer: '',
        technician: '',
      });
      document.getElementById("create-appointment-form").reset();
    }
  }

  //Notice that we can also replace multiple form change
  //eventListener functions with one
  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    //We can condense our form data event handling
    //into on function by using the input name to update it

    setFormData({
      //Previous form data is spread (i.e. copied) into our new state object
      ...formData,

      //On top of the that data, we add the currently engaged input key and value
      [inputName]: value
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create an appointment</h1>
          <form onSubmit={handleSubmit} id="create-appointment-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Automobile VIN" required type="text" name="vin" id="vin" className="form-control" />
              <label htmlFor="vin">Automobile VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Customer Name" required type="text" name="customer" id="customer" className="form-control" />
              <label htmlFor="customer">Customer Name</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleFormChange} placeholder="Date and Time" required type="datetime-local" name="date_time" id="date_time" className="form-control" />
                <label htmlFor="date_time">Date and Time</label>
            </div>
            <div className="mb-3">
                <select onChange={handleFormChange} required name="technician" id="technician" className="form-select">
                    <option value="">Choose a technician</option>

                    {technicians.map(technician => {
                        return (
                            <option key={technician.employee_id} value={technician.employee_id}>{technician.first_name} {technician.last_name}</option>
                        )
                })}
                </select>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
              <label htmlFor="reason">Reason</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default AppointmentForm;


-->
